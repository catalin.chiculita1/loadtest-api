package org.sample.loadtest;

import org.sample.loadtest.model.Department;
import org.sample.loadtest.model.Employee;
import org.sample.loadtest.repositories.DepartmentRepository;
import org.sample.loadtest.repositories.EmployeeRepository;
import org.sample.loadtest.service.ServiceC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseInitialization {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseInitialization.class);

    @Autowired
    private ServiceC serviceC;

    @Autowired
    private EmployeeRepository employeeRepository;

    @PostConstruct
    public void initialize() {
        LOGGER.info("Initializing database");
        initializeDepartments();
        initializeEmployees();
    }

    private void initializeDepartments() {
        LOGGER.info("Initializing departments");
        serviceC.createDepartment(new Department("logistics"));
        serviceC.createDepartment(new Department("human resources"));
        serviceC.createDepartment(new Department("sales"));
        serviceC.createDepartment(new Department("infrastructure"));
        serviceC.createDepartment(new Department("customer support"));
        LOGGER.info("Initialized departments");
    }


    private void initializeEmployees() {
        LOGGER.info("Initializing employees");
        serviceC.createEmployee(new Employee("Jack", 29l, 1l, 1500l));
        serviceC.createEmployee(new Employee("Ana", 25l, 1, 1550l));
        serviceC.createEmployee(new Employee("Diana", 31l, 2, 1700l));
        serviceC.createEmployee(new Employee("Andrew", 32l, 2, 1890l));
        serviceC.createEmployee(new Employee("Steve", 33l, 3, 2000l));
        serviceC.createEmployee(new Employee("Roger", 34l, 3, 2100l));
        serviceC.createEmployee(new Employee("Jasmine", 30l, 3, 2200l));
        serviceC.createEmployee(new Employee("Richard", 40l, 4, 2500l));
        serviceC.createEmployee(new Employee("Bruce", 29l, 5, 1800l));
        serviceC.createEmployee(new Employee("Bill", 19l, 5, 1750l));
        serviceC.createEmployee(new Employee("Nathalie", 27l, 5, 1900l));
        serviceC.createEmployee(new Employee("Jane", 25l, 5, 1900l));
        LOGGER.info("Initialized employees");
    }
}
/**
 * Employee4, "Andrew", 32, 2);
 * Employee5, "Steve", 33, 3);
 * Employee6, "Roger", 34, 3);
 * Employee7, "Jasmine", 30, 3);
 * Employee8, "Richard", 40, 4);
 * Employee9, "Bruce", 29, 5);
 * Employee10, "Bill", 19, 5);
 * Employee11, "Nathalie", 27, 5);
 * Employee12, "Jane", 25, 5);
 */
