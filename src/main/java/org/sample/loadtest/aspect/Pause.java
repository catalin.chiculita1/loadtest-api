package org.sample.loadtest.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Pause {
    /**
     * minimum time to wait
     *
     * @return
     */
    int min() default 100;

    /**
     * maximum time to wait
     *
     * @return
     */
    int max() default 200;
}
