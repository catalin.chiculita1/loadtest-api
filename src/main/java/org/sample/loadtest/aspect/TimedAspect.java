package org.sample.loadtest.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.UUID;

@Aspect
@Component
public class TimedAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimedAspect.class);

    @PostConstruct
    public void initialize() {
        LOGGER.info("Aspect initialized");
    }

    @Around("@annotation(Timed)")
    public Object measureExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger logger = LoggerFactory.getLogger(joinPoint.getClass());
        long start = System.currentTimeMillis();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Pause annotation = method.getAnnotation(Pause.class);
        if (annotation != null) {
            int min = annotation.min();
            int max = annotation.max();
            int timeToSleep = min + new Random().nextInt(max - min);
            Thread.sleep(timeToSleep);
        }

        String uuid = getUUID();

        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;
        logger.info("UUID={} -- {} execution={} ms", uuid, joinPoint.getSignature().toShortString(), executionTime);
        return proceed;
    }

    private String getUUID() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                    .getRequestAttributes()).getRequest();
            String uuid = request.getHeader("uuid");
            if (uuid != null && !uuid.isBlank()) {
                return uuid;
            }
        } catch (Exception e) {
//            LOGGER.warn("Unable to get the request: {}", e.getMessage());
        }
        return UUID.randomUUID().toString();
    }
}
