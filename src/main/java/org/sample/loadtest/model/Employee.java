package org.sample.loadtest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "employee")
@Data
public class Employee {

    public Employee(){

    }

    public Employee(String name, Long age, long departmentId, long salary) {
        this.name = name;
        this.age = age;
        this.department = new Department(departmentId);
        this.salary = salary;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMP_SEQ")
    private Long id;
    private String name;
    private Long age;
    @ManyToOne
    @JoinColumn(name = "department")
    private Department department;
    private Long salary;
}
