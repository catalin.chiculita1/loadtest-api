package org.sample.loadtest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "department")
@Data
public class Department {

    public Department() {
    }

    public Department(long id) {
        this.id = id;
    }

    public Department(String description) {
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPT_SEQ")
    private Long id;
    private String description;

}
