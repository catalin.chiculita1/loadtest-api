package org.sample.loadtest.rest;

import org.sample.loadtest.aspect.Timed;
import org.sample.loadtest.model.Department;
import org.sample.loadtest.service.ServiceA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/departments")
public class DepartmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    @Autowired
    private ServiceA serviceA;

    @Timed
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Department> getAllDepartments() {
        LOGGER.info("Retrieving departments");
        return serviceA.getDepartments();
    }

    @Timed
    @RequestMapping(value = "/:id", method = RequestMethod.GET)
    public Department getDepartmentById(@RequestParam("id") Long id) {
        LOGGER.info("Retrieving department by id {}", id);
        return serviceA.getDepartment(id);
    }

    @Timed
    @RequestMapping(value = "/:id", method = RequestMethod.PUT)
    public Department updateDepartmentById(@RequestParam("id") Long id, @RequestBody Department department) {
        LOGGER.info("Updating department by id {}", id);
        if (id != department.getId()) {
            LOGGER.warn("Invalid department details");
            return null;
        }

        return serviceA.updateDepartment(department);
    }

    @Timed
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Department createDepartment(@RequestBody Department department) {
        LOGGER.info("Creating department");
        return serviceA.createDepartment(department);
    }

}
