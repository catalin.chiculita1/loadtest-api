package org.sample.loadtest.rest;

import org.sample.loadtest.aspect.Timed;
import org.sample.loadtest.model.Employee;
import org.sample.loadtest.service.ServiceA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/employees")
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private ServiceA serviceA;

    @Timed
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Employee> getAllEmployees() {
        LOGGER.info("Retrieving Employees");
        return serviceA.getEmployees();
    }

    @Timed
    @RequestMapping(value = "/:id", method = RequestMethod.GET)
    public Employee getEmployeeById(@RequestParam("id") Long id) {
        LOGGER.info("Retrieving Employee by id {}", id);
        return serviceA.getEmployee(id);
    }

    @Timed
    @RequestMapping(value = "/:id", method = RequestMethod.PUT)
    public Employee updateEmployeeById(@RequestParam("id") Long id, @RequestBody Employee Employee) {
        LOGGER.info("Updating Employee by id {}", id);
        if (id != Employee.getId()) {
            LOGGER.warn("Invalid Employee details");
            return null;
        }

        return serviceA.updateEmployee(Employee);
    }

    @Timed
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody Employee Employee) {
        LOGGER.info("Creating Employee");
        return serviceA.createEmployee(Employee);
    }

    @Timed
    @RequestMapping(value = "/salary", method = RequestMethod.PUT)
    public List<Employee> updateSalaries(@RequestParam(name = "department", required = false) Long department,
                                         @RequestParam("percent") Long percent) {
        LOGGER.info("Updating salaries with {}% for department {}", percent, department != null ? department : "all");
        List<Employee> employees = serviceA.getEmployees();
        if (department != null) {
            employees.stream().filter(e -> e.getDepartment().getId().equals(department)).forEach(e -> e.setSalary(e.getSalary() * percent / 100));
        } else {
            employees.forEach(e -> e.setSalary(e.getSalary() * percent / 100));
        }

        return serviceA.updateEmployees(employees);
    }
}
