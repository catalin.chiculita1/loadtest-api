package org.sample.loadtest.service;

import org.sample.loadtest.aspect.Pause;
import org.sample.loadtest.aspect.Timed;
import org.sample.loadtest.model.Department;
import org.sample.loadtest.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceB {

    @Autowired
    private ServiceC serviceC;

    @Timed
    public Department getDepartment(Long id) {
        return serviceC.getDepartment(id);
    }

    @Pause
    @Timed
    public List<Department> getDepartments() {
        return serviceC.getDepartments();
    }

    @Timed
    public Department updateDepartment(Department department) {
        return serviceC.updateDepartment(department);
    }

    @Timed
    public Department createDepartment(Department department) {
        return serviceC.createDepartment(department);
    }

    @Timed
    public Employee getEmployee(Long id) {
        return serviceC.getEmployee(id);
    }

    @Timed
    public List<Employee> getEmployees() {
        return serviceC.getEmployees();
    }

    @Timed
    public Employee updateEmployee(Employee employee) {
        return serviceC.updateEmployee(employee);
    }

    @Timed
    public Employee createEmployee(Employee employee) {
        return serviceC.createEmployee(employee);
    }

    @Timed
    public List<Employee> updateEmployees(List<Employee> employees){
        return serviceC.updateEmployees(employees);
    }
}
