package org.sample.loadtest.service;

import org.sample.loadtest.aspect.Pause;
import org.sample.loadtest.aspect.Timed;
import org.sample.loadtest.model.Department;
import org.sample.loadtest.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceA {

    @Autowired
    private ServiceB serviceB;

    @Timed
    public Department getDepartment(Long id) {
        return serviceB.getDepartment(id);
    }

    @Pause
    @Timed
    public List<Department> getDepartments() {
        return serviceB.getDepartments();
    }

    @Timed
    public Department updateDepartment(Department department) {
        return serviceB.updateDepartment(department);
    }

    @Timed
    public Department createDepartment(Department department) {
        return serviceB.createDepartment(department);
    }

    @Timed
    public Employee getEmployee(Long id) {
        return serviceB.getEmployee(id);
    }

    @Timed
    public List<Employee> getEmployees() {
        return serviceB.getEmployees();
    }

    @Timed
    public Employee updateEmployee(Employee employee) {
        return serviceB.updateEmployee(employee);
    }

    @Timed
    public Employee createEmployee(Employee employee) {
        return serviceB.createEmployee(employee);
    }

    @Timed
    public List<Employee> updateEmployees(List<Employee> employees){
        return serviceB.updateEmployees(employees);
    }
}
