package org.sample.loadtest.service;

import org.sample.loadtest.aspect.Pause;
import org.sample.loadtest.aspect.Timed;
import org.sample.loadtest.model.Department;
import org.sample.loadtest.model.Employee;
import org.sample.loadtest.repositories.DepartmentRepository;
import org.sample.loadtest.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceC {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Timed
    public Department getDepartment(Long id) {
        return departmentRepository.findById(id).orElse(null);
    }

    @Pause
    @Timed
    public List<Department> getDepartments() {
        return departmentRepository.findAll();
    }

    @Timed
    public Department updateDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Timed
    public Department createDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Timed
    public Employee getEmployee(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Timed
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @Timed
    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Timed
    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Timed
    public List<Employee> updateEmployees(List<Employee> employees) {
        return employeeRepository.saveAll(employees);
    }
}
